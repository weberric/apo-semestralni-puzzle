#ifndef LEDS
#define LEDS

#include "std_libs.h"
#include "local_libs.h"
#include "constants.h"

/* Parses the 32bit mask bit by bit and changes the status
   of LED line so that every bit corresponds to an LED. 
   0: Turned off
   1: Turned on
   */
void line_render_mask(unsigned char* mem_base, uint32_t mask);

/* Generates a mask that turns on N LEDs in the line, so that
   (current/maximum) = (N/32), to approximate the ratio of tiles
   being in the correct place.*/
uint32_t line_generate_mask(uint8_t current, uint8_t maximum);

/* Divides the 32bit int into 4 (3) chars to control R, G and B separately.
   First bytes of the int is unused.

   Format: uint32_t rgb = 0x--RRGGBB */
void rgb_render_color(unsigned char* mem_base, uint32_t rgb);


#endif