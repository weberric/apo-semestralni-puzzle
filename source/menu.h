#ifndef MENU
#define MENU

#include "std_libs.h"
#include "constants.h"

typedef struct menu {
    int8_t cursor;
    bool selected;
    uint8_t board;
    uint8_t file;
    uint8_t randomizer;
    char* file_names[MENU_IMG_COUNT]; // unfinished image support
    char* button_names[MENU_BUT_COUNT];
} menu_t;

/* Allocates memory for a menu, initializes default values. */
menu_t* create_menu(void);
/* Adjusts menu cursor and text after getting rotation info. */
bool update_menu_rot(menu_t* menu, bool cw, bool ccw);
/* Adjusts menu button strings. */
void update_buttons_text(menu_t* menu);
/* Prints the menu in terminal. */
void print_menu(menu_t* menu);

#endif