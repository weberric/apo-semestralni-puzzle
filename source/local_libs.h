#ifndef LOCALS
#define LOCALS

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#include "model.h"
#include "menu.h"
#include "graphics.h"
#include "leds.h"
#include "knobs.h"

#endif