#include "app.h"

/* GLOBAL VARS */
    // membases
unsigned char* knob_membase;
unsigned char* line_membase;
unsigned char* rgb1_membase;
unsigned char* rgb2_membase;
unsigned char* parlcd_membase;
    // controller globals
uint8_t rot;
bool r_push, g_push, b_push;
bool r_click, g_click, b_click;
bool r_move_cw, g_move_cw, b_move_cw, r_move_ccw, g_move_ccw, b_move_ccw;
uint8_t r_last, g_last, b_last;
    // RGB control globals
uint32_t rgb1_color, rgb2_color;
uint32_t pulse_counter = 0;

int main(int argc, char *argv[]) {
    // membases init
    knob_membase = map_phys_address(SPILED_REG_BASE_PHYS + SPILED_REG_KNOBS_8BIT_o, 4, 0);
    line_membase = map_phys_address(SPILED_REG_BASE_PHYS + SPILED_REG_LED_LINE_o, 4, 0);
    rgb1_membase = map_phys_address(SPILED_REG_BASE_PHYS + SPILED_REG_LED_RGB1_o , 3, 0);
    rgb2_membase = map_phys_address(SPILED_REG_BASE_PHYS + SPILED_REG_LED_RGB2_o, 3, 0);
    parlcd_membase = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (knob_membase == NULL || 
        line_membase == NULL || 
        rgb1_membase == NULL || 
        rgb2_membase == NULL || 
        parlcd_membase == NULL) {
        fprintf(stderr, "Error: Couldn't map physical address!\n");
        exit(104);
    }   

    // knobs init 
    r_push = false, g_push = false, b_push = false;
    r_last = get_knob_rotation(knob_membase, KNOB_RED);
    g_last = get_knob_rotation(knob_membase, KNOB_GREEN);
    b_last = get_knob_rotation(knob_membase, KNOB_BLUE);

    // LEDs init 
    uint32_t mask = 0; // 32 bits - each one is a LED

    // model init
    srand(time(0)); // init random seed based on time
    board_t* board = NULL; // will be init after getting board_size
    bool progress_made;

    // LCD init
    font_descriptor_t* font_d; // for font printing
    uint16_t fb[DISPLAY_HEIGHT * DISPLAY_WIDTH] = {0}; // frame buffer (holds colors of pixels)
    font_d = &font_winFreeSystem14x16;     

    // menu init
    menu_t* menu = create_menu();
    bool menu_change = false;
    bool shutdown = false;

    // --------------------------------------------------------
    // MAIN LOOP
    while(!shutdown) {
        // start menu
        menu->selected = false;
        update_buttons_text(menu);
        print_menu(menu);
        draw_menu(menu, fb, font_d);
        update_display(parlcd_membase, fb);
        
        // menu cycle
        while (!shutdown) {
            get_button_input();
            get_turn_input();

            menu_change = false;
            rgb1_color = RGB_OFF;

            // only blue knob is used, so LEDs can only use blue color
            if (b_push) {
                rgb1_color = RGB_BLUE;
            }
            // click is triggered on release
            if (b_click) {
                menu->selected = !menu->selected;
                menu_change = true;
            }
            
            rgb_render_color(rgb1_membase, rgb1_color);
            if (menu->cursor == MENU_START_BUT && menu->selected) {
                printf("Starting game now!\n");
                menu_change = true;
                break;
            }
            if (menu->cursor == MENU_STOP_BUT && menu->selected) {
                printf("Ending game now!\n");
                shutdown = true;
                break;
            }

            rgb2_color = RGB_OFF;
            menu_change = update_menu_rot(menu, b_move_cw, b_move_ccw);

            // update menu
            if (menu_change) {
                rgb2_color = RGB_BLUE;
                update_buttons_text(menu);
                print_menu(menu);
                draw_menu(menu, fb, font_d);
                update_display(parlcd_membase, fb);
            }
            update_RGB_pulse();
            usleep(1000 * TICK_LEN_MS);
        }

        //---------------------------------------------------------------------
        if (shutdown) break;

        // start a new level
        if (board != NULL) {
            free(board);
        }
        board = create_board(menu->board);
        int difficulty = (board->max_idx + 1) * menu->randomizer;
        if (!init_board(board)) {
            fprintf(stderr, "Error: Board not created in model!\n");
            exit(102);
        }
        printf("Board created with side len: %u\n", board->side_len);
        shuffle_board(board, difficulty);
        printf("Board shuffled %d times!\n", difficulty);
        print_board(board);
        color_display(LCD_BLACK, fb);
        draw_board(board, fb, font_d);
        update_display(parlcd_membase, fb);
        mask = line_generate_mask(get_progress(board), board->max_idx);
        line_render_mask(line_membase, mask);

        // ----------------------------------------------------------------
        // GAME CYCLE
        while (!shutdown) {
            get_button_input();
            get_turn_input();

            // RGB LED button output
            rgb1_color = RGB_OFF;
            if (r_push)
                rgb1_color += RGB_RED;
            if (g_push)
                rgb1_color += RGB_GREEN;
            if (b_push)
                rgb1_color += RGB_BLUE;
            rgb_render_color(rgb1_membase, rgb1_color);

            // RGB LED button and MODEL output
            rgb2_color = RGB_OFF;
            progress_made = false;

            if (r_move_cw) {
                if(push_dir(board, DIR_DOWN, true)) {
                    progress_made = true;
                }
                rgb2_color = RGB_RED;
            } else if (r_move_ccw) {
                if(push_dir(board, DIR_UP, true)) {
                    progress_made = true;
                }
                rgb2_color = RGB_RED;
            }
            if (g_move_cw) {
                if(push_dir(board, DIR_RIGHT, true)) {
                    progress_made = true;
                }
                rgb2_color = RGB_GREEN;
            } else if (g_move_ccw) {
                if(push_dir(board, DIR_LEFT, true)) {
                    progress_made = true;
                }
                rgb2_color = RGB_GREEN;
            }
                
            if (progress_made) {
                print_board(board);
                color_display(LCD_BLACK, fb);
                draw_board(board, fb, font_d);
                update_display(parlcd_membase, fb);
                mask = line_generate_mask(get_progress(board), board->max_idx);
                line_render_mask(line_membase, mask);

                // when solved, the game blocks input and starts blinking white
                if (mask == 0xFFFFFFFF) {
                    printf("PUZZLE SOLVED!\n");
                    for (int i = 0; i < 5; i++) {
                        rgb_render_color(rgb1_membase, RGB_WHITE);
                        rgb_render_color(rgb2_membase, RGB_WHITE);
                        usleep(1000 * 250); // 250 ms
                        rgb_render_color(rgb1_membase, RGB_OFF);
                        rgb_render_color(rgb2_membase, RGB_OFF);
                        usleep(1000 * 250); // 250 ms
                    }
                }
            }
            // update pulse on turning, except after winning
            if (mask != 0xFFFFFFFF) {
                update_RGB_pulse();
            }
            if (b_click) {
                printf("Exiting level...\n");
                break;
            }
            // turnoff when all pressed
            if (r_push && g_push && b_push) {
                printf("All buttons pushed, ending now...\n");
                shutdown = true;
                break;
            }

            // timer
            usleep(1000 * TICK_LEN_MS);
        }

        // reset lvl
        rgb_render_color(rgb1_membase, RGB_OFF);
        rgb_render_color(rgb2_membase, RGB_OFF);
        line_render_mask(line_membase, 0);
    }

    // main loop exit, clean up
    rgb_render_color(rgb1_membase, RGB_OFF);
    rgb_render_color(rgb2_membase, RGB_OFF);
    line_render_mask(line_membase, 0);
    color_display(LCD_BLACK, fb);
    update_display(parlcd_membase, fb);

    free(menu);
    free(board->tile_order);
    free(board);
    // ----------------------------------------------------------------------

    return 0;
}

void get_button_input(void) {
    // red button check
    r_click = false;
    if (is_button_pushed(knob_membase, KNOB_RED)) {
        if (!r_push) {
            r_push = true;
        }  
    } else {
        if (r_push) {
            r_push = false;
            r_click = true;
        }
    }
    // green button check
    g_click = false;
    if (is_button_pushed(knob_membase, KNOB_GREEN)) {
        if (!g_push) {
            g_push = true;
        }  
    } else {
        if (g_push) {
            g_push = false;
            g_click = true;
        }
    }
    // blue button check  
    b_click = false;     
    if (is_button_pushed(knob_membase, KNOB_BLUE)) {
        if (!b_push) {
            b_push = true;
        }  
    } else {
        if (b_push) {
            b_push = false;
            b_click = true;
        }
    }
}

void get_turn_input(void) {
    r_move_cw = false, g_move_cw = false, b_move_cw = false;
    r_move_ccw = false, g_move_ccw = false, b_move_ccw = false;
    // red turn check
    rot = get_knob_rotation(knob_membase, KNOB_RED);
    if (rot - r_last == 4 || r_last - rot == 252) {
        r_last = rot;
        r_move_cw = true;
    } else if (r_last - rot == 4 || rot - r_last == 252) {
        r_last = rot;
        r_move_ccw = true;
    }
    // green turn check
    rot = get_knob_rotation(knob_membase, KNOB_GREEN);
    if (rot - g_last == 4 || g_last - rot == 252) {
        g_last = rot;
        g_move_cw = true;
    } else if (g_last - rot == 4 || rot - g_last == 252) {
        g_last = rot;
        g_move_ccw = true;
    }
    // blue turn check
    rot = get_knob_rotation(knob_membase, KNOB_BLUE);
    if (rot - b_last == 4 || b_last - rot == 252) {
        b_last = rot;
        b_move_cw = true;
    } else if (b_last - rot == 4 || rot - b_last == 252) {
        b_last = rot;
        b_move_ccw = true;
    }
}

void update_RGB_pulse(void) {
    if (rgb2_color != RGB_OFF) {
        rgb_render_color(rgb2_membase, rgb2_color);
        pulse_counter = 0;
    } else if (pulse_counter == 0) {
        // if counter is reset (a color was turned on for the full duration)
        // then turn it off
        rgb_render_color(rgb2_membase, rgb2_color);
    }
    // update counter
    pulse_counter = (pulse_counter + 1) % (PULSE_WINDOW_MS / TICK_LEN_MS);    
}
