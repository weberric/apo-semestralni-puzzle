#include "graphics.h"

void draw_pixel(int x, int y, unsigned short color, int scale, uint16_t* fb) {
  int i,j;
  // how big pixel is depend on scale (scale x scale size)
  for (i = 0; i < scale; i++) {
    for (j = 0; j < scale; j++) {
      if (x + i >= 0 && x + i < DISPLAY_WIDTH && y + j >= 0 && y + j < DISPLAY_HEIGHT) {
        // Changing color on coordinate in frame buffer
        fb[x + i + DISPLAY_WIDTH * (y + j)] = color;
      }
    }
  }
}

bool draw_char(int x, int y, char ch, uint16_t color, int scale, font_descriptor_t* font_d, uint16_t* fb) {
  // default or custom width for character
  int width = !font_d->width ? font_d->maxwidth : font_d->width[ch - font_d->firstchar];
  // if character exists in font
  if (ch < font_d->firstchar || ch >= font_d->firstchar + font_d->size) {
    return false;
  }
  // pointer on start of character bitmap  
  const font_bits_t *ptr = &font_d->bits[(ch - font_d->firstchar) * font_d->height];
  int i, j;
  // go through bitmap of character
  for (i = 0; i < font_d->height; i++, ptr++) {
    font_bits_t val = *ptr;
    for (j = 0; j < width; j++, val <<= 1) {
      // if most significant bit is set draw pixel on this coordinate(from bitmap)
      if (val & 0x8000)
        draw_pixel(x + scale * j, y + scale * i, color, scale, fb);
    }
  }
  return true;
}


void draw_tile(int x, int y, int size, char* char_tile, uint16_t *fb, uint16_t color, font_descriptor_t* font_d) {
  // draw tile border
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      if (i == 0 || j == 0 || i == size - 1 || j == size - 1){
        fb[(y + i) * DISPLAY_WIDTH + (x + j)] = color;
      }
    }
  }
  // calculate width offset to centralize number on tile
  int width = !font_d->width ? font_d->maxwidth : font_d->width[char_tile[0] - font_d->firstchar];
  int width_offset = (size - (width * SCALE_DEFAULT * 2 + 1))/2;
  int char_x_add = (width_offset <= 1) ? 1 : width_offset;
  // coordinates to place number with centralization
  int char_x = x + char_x_add;
  int char_y = y + (size - font_d->height * SCALE_DEFAULT)/2;
  // go through number array and write decimals
  for(int i = 0; i < strlen(char_tile); ++i) {
    // draw one character
    draw_char(char_x, char_y, char_tile[i], color, SCALE_DEFAULT, font_d, fb);
    // Add offset for coordinates of next character of number
    width = !font_d->width ? font_d->maxwidth : font_d->width[char_tile[i] - font_d->firstchar];
    char_x += width * SCALE_DEFAULT + 1;
  }
}

void color_display(uint16_t color, uint16_t* fb) {
    for (int i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
    // updating pixels colors in memory
        fb[i] = color;
    }
}

void draw_board(board_t *board, uint16_t* fb, font_descriptor_t* font_d) {
  // transform tile numbers into strings
  char tiles[board->max_idx + 1][3];
  for (int i = 0; i < board->max_idx + 1; ++i){
    if (board->tile_order[i] == board->max_idx) {
      sprintf(tiles[i], "  ");
    } else {
      sprintf(tiles[i], "%2u", board->tile_order[i] + 1);
    }
    if(tiles[i] == NULL) {
      fprintf(stderr, "Error: tile_digits[%d] is null!\n", i);
      exit(103);
    }
  }

  uint16_t color;
  // count offsets for whole board on display and tile side size
  int vertical_offset = BOARD_VERT_OFFSET;
  int puzzle_height = DISPLAY_HEIGHT - 2 * vertical_offset;
  int max_tile_size = puzzle_height / board->side_len;
  int puzzle_width = max_tile_size * board->side_len;
  int horizontal_offset = (DISPLAY_WIDTH - puzzle_width) / 2;
  // go through tiles on board and draw for each tile with number
  for (int i = 0; i < board->side_len; ++i) {
    for (int j = 0; j < board->side_len; ++j) {
      // when a tile is in the right place
      if (board->tile_order[i * board->side_len + j] == (i * board->side_len + j)) {
        color = LCD_GREEN;
      } else {
        color = LCD_RED;
      }
      int tile_x = horizontal_offset + j * max_tile_size;
      int tile_y = vertical_offset + i * max_tile_size;
      draw_tile(tile_x, tile_y, max_tile_size, tiles[i * board->side_len + j], fb, color, font_d);
    }
  }
}

void update_display(unsigned char* membase, uint16_t* fb) {
  // command to write on memory buffer
  parlcd_write_cmd(membase, 0x2c);
  for (int i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
    // updating pixels colors in memory
        parlcd_write_data(membase, fb[i]);
    }
}

void draw_menu(menu_t *menu, uint16_t* fb, font_descriptor_t* font_d) {
  // Make display black
  color_display(LCD_BLACK, fb);
  // Change scale and initialize offset
  int vert_offset = 119;
  int between_rows = 3;
  int horizon_offset = 86; 
  int scale = SCALE_DEFAULT;
  // Print on display whole menu
  for (int i = 0; i < MENU_BUT_COUNT; i++) {
    int char_x = horizon_offset;
    // Change position of row
    int char_y = (vert_offset / scale + (font_d->height * i * scale) + (between_rows * i * scale));

    // draw a cursor before the row if it there's a cursor
    char ch = '*';
    if (i == menu->cursor) {
      draw_char(char_x, char_y, ch, LCD_BLUE, scale, font_d, fb);
    }
    int width = !font_d->width ? font_d->maxwidth : font_d->width[ch - font_d->firstchar];
    width *= scale;
    // move to the next char
    char_x += width + 1;
    // draw the string char by char
    for (int j = 0; j < MENU_MAX_STR_LEN; ++j) {
      ch = menu->button_names[i][j];
      draw_char(char_x, char_y, ch, LCD_WHITE, scale, font_d, fb);
      width = !font_d->width ? font_d->maxwidth : font_d->width[ch - font_d->firstchar];
      width *= scale;
      char_x += width + 1;
    }  
  }
}
