#ifndef MODEL
#define MODEL

#include "std_libs.h"
#include "constants.h"


typedef struct board {
    uint8_t side_len; // size == side_len * side_len
    uint8_t max_idx; // last index in array - empty hole at this spot
    uint8_t *tile_order; // unsigned char array - holds current order of tiles
} board_t;

/* Makes a 2D array of tiles with given size.
   The size must be between 2 and 8. */
board_t *create_board(uint8_t size);
/* Takes board pointer as arguments.
   Fills board with tiles in order
   then shuffles them. */
bool init_board(board_t *board);
/* Iteratively calls swap_tiles 'perm' number of times
   to shuffle the puzzle board. This way is it guaranteed 
   that the puzzle is solvable.*/
void shuffle_board(board_t *board, uint32_t perm);
/* Swaps the order of two tiles. */
void swap_tiles(board_t *board, uint8_t pos1, uint8_t pos2);
/* Gets direction as defined char,
   swaps two adjacent tiles.
   Prints output into console when print_output is true.
   Returns false on invalid move.*/
bool push_dir(board_t *board, uint8_t direction, bool print_output);
/* Returns a random int between
   0 and 'upper_limit' (inclusive). */
uint32_t get_randint(uint32_t upper_limit);
/* Prints the current board state to console. */
void print_board(board_t *board);
/* Returns the number of tiles on board
   that are in the right place.*/
uint8_t get_progress(board_t* board);
/* Checks if the board is solved (in correct order).
   Returns true is solved, false if not. */
bool is_solved(board_t *board);

#endif