#include "menu.h"

menu_t* create_menu(void) {
    menu_t* ret = (menu_t*)malloc(sizeof(menu_t));
    if (ret == NULL) {
        fprintf(stderr, "Error: Menu malloc failed!");
        exit(101);
    }

    // init all as zero

    for (int i = 0; i < MENU_BUT_COUNT; i++) {
        ret->button_names[i] = (char*)malloc(MENU_MAX_STR_LEN * sizeof(char));
        if (ret->button_names[i] == NULL) {
            fprintf(stderr, "Error: Menu string malloc failed!");
            exit(101);
        }
    }
    for (int i = 0; i < MENU_IMG_COUNT; i++) {
        ret->file_names[i] = "";
    }      
    ret->cursor = MENU_START_BUT; // default
    ret->selected = false; // default
    ret->board = MIN_BOARD_SIZE; // default
    ret->file = 0; // default
    ret->randomizer = 0; // default
    ret->button_names[MENU_START_BUT] = "PLAY GAME"; // doesn't change
    ret->button_names[MENU_STOP_BUT] = "TURN OFF";  // doesn't change

    return ret;
}

bool update_menu_rot(menu_t* menu, bool cw, bool ccw) {
    bool menu_changed = false;
    if (cw) {
        if (!menu->selected) {
            menu->cursor = (menu->cursor + 1) % MENU_BUT_COUNT;
            menu_changed = true;
        } else {
            switch (menu->cursor)
            {
            case MENU_BOARD_BUT:
                menu->board = (menu->board == MAX_BOARD_SIZE) ? MIN_BOARD_SIZE : menu->board + 1;
                menu_changed = true;
                break;
            case MENU_IMAGE_BUT:
                menu->file = (menu->file + 1) % MENU_IMG_COUNT;
                menu_changed = true;
                break;
            case MENU_RANDOM_BUT:
                menu->randomizer = (menu->randomizer + 1) % MENU_RND_COUNT;
                menu_changed = true;
                break;
            default:
                // do nothing
                break;
            }
        }
    } else if (ccw) {
        if (!menu->selected) {
            menu->cursor = (menu->cursor == 0) ? MENU_BUT_COUNT - 1 : menu->cursor - 1;
            menu_changed = true;
        } else {
            switch (menu->cursor)
            {
            case MENU_BOARD_BUT:
                menu->board = (menu->board == MIN_BOARD_SIZE) ? MAX_BOARD_SIZE : menu->board - 1;
                menu_changed = true;
                break;
            case MENU_IMAGE_BUT:
                menu->file = (menu->file == 0) ? MENU_IMG_COUNT - 1 : menu->file - 1;
                menu_changed = true;
                break;
            case MENU_RANDOM_BUT:
                menu->randomizer =(menu->randomizer == 0) ? MENU_RND_COUNT - 1 : menu->randomizer - 1;
                menu_changed = true;
                break;
            default:
                // do nothing
                break;
            }
        }          
    }
    return menu_changed;
}

void update_buttons_text(menu_t* menu) {
    sprintf(menu->button_names[MENU_BOARD_BUT], "Board size: %ux%u", menu->board, menu->board);
    sprintf(menu->button_names[MENU_IMAGE_BUT], "Image: %s", menu->file_names[menu->file]);
    sprintf(menu->button_names[MENU_RANDOM_BUT], "Randomizer: %d", menu->randomizer);
}

void print_menu(menu_t* menu) {
    int i, j;
    for (i = 0; i < MENU_MAX_STR_LEN + 4; i++) {
        putchar('-');
    }
    putchar('\n');
    for (i = 0; i < MENU_BUT_COUNT; i++) {
        putchar('|');
        printf("%s", menu->button_names[i]);
        for (int j = strlen(menu->button_names[i]); j < MENU_MAX_STR_LEN; j++) {
            putchar(' ');
        }
        if (i == menu->cursor) {
            printf(" *|\n");
        } else {
            printf("  |\n");
        }
        for (j = 0; j < MENU_MAX_STR_LEN + 4; j++) {
            putchar('-');
        }
        putchar('\n');
    }
    putchar('\n');
}
