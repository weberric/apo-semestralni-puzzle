#ifndef APP
#define APP

#include "std_libs.h"
#include "local_libs.h"
#include "constants.h"

/* Reads and changes global control bools for buttons.
   Adjusts them so that the app loop can detect input.*/
void get_button_input(void);

/* Reads and changes global control bools for rotating knobs.
   Adjusts them so that the app loop can detect input.*/
void get_turn_input(void);

/* Solution for pulse lighting up of the LED.
   After rotating a knob, the RBG LED lights up with the corresponding
   color for PULSE_WINDOW_MS milliseconds
   (refreshes every time a knob is turned). */
void update_RGB_pulse(void);

#endif