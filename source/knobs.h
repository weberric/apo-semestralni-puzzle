#ifndef KNOBS
#define KNOBS

#include "std_libs.h"
#include "local_libs.h"
#include "constants.h"

/* Returns true if given button is pushed down, false otherwise. */
bool is_button_pushed(unsigned char* mem_base, uint8_t knob_color);

/* Return filtered position of given knob as 8bit char. */
uint8_t get_knob_rotation(unsigned char* mem_base, uint8_t knob_color);

#endif