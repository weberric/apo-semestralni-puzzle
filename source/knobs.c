#include "knobs.h"

bool is_button_pushed(unsigned char* mem_base, uint8_t knob_color) {
    unsigned char reg_location = mem_base[3];
    if (((reg_location >> knob_color) & 0x01) == 1) {
        return true;
    }
    return false;
}

uint8_t get_knob_rotation(unsigned char* mem_base, uint8_t knob_color) {
    unsigned char reg_location = mem_base[knob_color];
    return reg_location;
}
