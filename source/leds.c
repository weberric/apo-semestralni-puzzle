#include "leds.h"

void line_render_mask(unsigned char* mem_base, uint32_t mask) {
    unsigned char region; // one fourth of the entire line
    int i = 0;
    for (i = 0; i < LED_SUBLINES; i++) {
        // uint32_t -> unsigned char[4] conversion
        region = (mask >> 8 * i) & 0xff;
        mem_base[i] = region;
    } 
}

uint32_t line_generate_mask(uint8_t current, uint8_t maximum) {
    uint32_t mask = 0;
    float cur = (float) current;
    float max = (float) maximum;
    float progress = cur / max; // is a decimal number between 0 and 1
    uint32_t led_count = (uint32_t) (LED_LINE_LEN * progress); 

    if (led_count > LED_LINE_LEN) {
        fprintf(stderr, "Error: LED count is out of range!\n");
        exit(101);
    }

    for (int i = 0; i < led_count; i++) {
        mask |= (1 << i);
    }
    return mask;
}

void rgb_render_color(unsigned char* mem_base, uint32_t rgb) {
    // tranforms an int into 4 chars
    // sets registers for R, G and B respectively
    for (int i = 2; i >= 0; i--) {
        mem_base[i] = (rgb >> 8 * i) & 0xff;
    }
}
