#ifndef CONSTANTS
#define CONSTANTS

#define _POSIX_C_SOURCE 200112L

// LCD display colors
#define LCD_BLACK           0x0000
#define LCD_WHITE           0xFFFF
#define LCD_RED             0xf800
#define LCD_GREEN           0x07c0
#define LCD_BLUE            0x001f
// LCD display properties for graphics
#define DISPLAY_WIDTH       480
#define DISPLAY_HEIGHT      320
#define SCALE_DEFAULT       2
#define BOARD_VERT_OFFSET   20
// push directions for model
#define DIR_UP              0
#define DIR_RIGHT           1
#define DIR_DOWN            2
#define DIR_LEFT            3
// model board size limits
#define MIN_BOARD_SIZE      2
#define MAX_BOARD_SIZE      8
// physical properties of the LED line
#define LED_LINE_LEN        32
#define LED_SUBLINES        4
// predefined colors for RGB LEDs
#define RGB_OFF             0x00000000
#define RGB_WHITE           0x00FFFFFF
#define RGB_RED             0x00FF0000
#define RGB_GREEN           0x0000FF00
#define RGB_BLUE            0x000000FF
// modifiers to differentiate knobs
#define KNOB_RED            2
#define KNOB_GREEN          1
#define KNOB_BLUE           0
// time constants for controlling LEDs
#define TICK_LEN_MS         5
#define PULSE_WINDOW_MS     500
// menu macros
#define MENU_BUT_COUNT      5
#define MENU_IMG_COUNT      3 // unused for now
#define MENU_RND_COUNT      10
#define MENU_MAX_STR_LEN    20
// menu buttons fake enum
#define MENU_START_BUT      0
#define MENU_BOARD_BUT      1
#define MENU_IMAGE_BUT      2
#define MENU_RANDOM_BUT     3
#define MENU_STOP_BUT       4

#endif