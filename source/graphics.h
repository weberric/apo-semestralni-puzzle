#ifndef GRAPHICS
#define GRAPHICS

#include "std_libs.h"
#include "constants.h"
#include "local_libs.h"

/* Draw pixel on coordinate(scalable) */
void draw_pixel(int x, int y, unsigned short color, int scale, uint16_t* fb);

/* Draw character on coordinate */
bool draw_char(int x, int y, char ch, uint16_t color, int scale, font_descriptor_t* font_d, uint16_t* fb);

/* Draw one tile from board with number */
void draw_tile(int x, int y, int size, char* char_tile, uint16_t* fb, uint16_t color, font_descriptor_t* font_d);

/* Set one color to the entire display */
void color_display(uint16_t color, uint16_t* fb);

/* Draw whole board with numbers */
void draw_board(board_t* board, uint16_t* fb, font_descriptor_t* font_d);

/* Mapping for memory buffers(updates display) */
void update_display(unsigned char* membase, uint16_t* fb);

/* Prepare to draw board and draw it */
void draw_display(board_t *board);

/* Drawing menu on display */
void draw_menu(menu_t* menu, uint16_t* fb, font_descriptor_t* font_d);

#endif
