#include "model.h"

board_t* create_board(uint8_t size) {
    board_t* ret = NULL;
    // check correct board size
    if (size < MIN_BOARD_SIZE || size > MAX_BOARD_SIZE) {
        fprintf(stderr, "Error: Given board size out of range (must be 2 - 8)!\n");
        return ret;
    }
    // allocate space for board
    ret = (board_t*) malloc(sizeof(board_t));
    if (ret == NULL) {
        fprintf(stderr, "Error: Board malloc failed!");
        exit(101);
    }
    // allocate space for tile order array
    ret->tile_order = (uint8_t*) malloc(size * size * sizeof(uint8_t));
    if (ret->tile_order == NULL) {
        fprintf(stderr, "Error: Order array malloc failed!");
        exit(101);
    }
    ret->side_len = size;
    ret->max_idx = (size * size) - 1;
    return ret;
}

bool init_board(board_t* board) {
    if (board == NULL) {
        printf("Error: Init unsuccessful!\n");
        return false;
    }
    uint8_t size = board->max_idx + 1; 
    for (uint8_t i = 0; i < size; i++) {
        board->tile_order[i] = i;
    }
    printf("Init successful!\n");
    return true;
}

uint32_t get_randint(uint32_t upper_limit) {
    return (uint32_t) (rand() % (upper_limit + 1));
}

void print_board(board_t* board) {
    for (int i = 0; i < 3 * board->side_len + 1; i++) {
        putchar('-');
    }
    putchar('\n');
    for (int i = 0; i < board->side_len; i++) {
        putchar('|');
        for (int j = 0; j < board->side_len; j++) {
            uint8_t tmp = board->tile_order[i * board->side_len + j];
            if (tmp == board->max_idx) {
                printf("  |");
            } else {
                printf("%2u|", tmp + 1);
            }
        }
        putchar('\n');
    }

    for (int i = 0; i < 3 * board->side_len + 1; i++) {
        putchar('-');
    }
    putchar('\n');
}

void shuffle_board(board_t* board, uint32_t perm) {
    uint32_t dir = get_randint(3); // 0 to 3
    if (perm == 0) {
        return;
    }
    if (!push_dir(board, dir, false)) {
        perm++;
    }
    shuffle_board(board, perm - 1);
}

void swap_tiles(board_t* board, uint8_t pos1, uint8_t pos2) {
    uint8_t tmp = board->tile_order[pos1];
    board->tile_order[pos1] = board->tile_order[pos2];
    board->tile_order[pos2] = tmp;
}

bool push_dir(board_t* board, uint8_t direction, bool print_output) {
    uint8_t idx = 0;
    bool ret = false;
    for (int i = 0; i <= board->max_idx; i++) {
        if (board->tile_order[i] == board->max_idx) {
            idx = i;
            break;
        }
    }

    switch (direction) {
    case DIR_UP:
        if (idx < board->side_len * (board->side_len - 1)) {
            if (print_output) 
                printf("Pushed a tile up!\n");
            swap_tiles(board, idx, idx + board->side_len);
            ret = true;
        } else {
            if (print_output)
                printf("Invalid move!\n");
        }
        break;
    case DIR_RIGHT:
        if ((idx % board->side_len) != 0) {
            if (print_output) 
                printf("Pushed a tile to the right!\n");
            swap_tiles(board, idx, idx - 1);
            ret = true;
        } else {
            if (print_output)
                printf("Invalid move!\n");
        }
        break;
    case DIR_DOWN:
        if (idx >= board->side_len) {
            if (print_output) 
                printf("Pushed a tile down!\n");
            swap_tiles(board, idx, idx - board->side_len);
            ret = true;
        } else {
            if (print_output)
                printf("Invalid move!\n");
        }
        break;
    case DIR_LEFT:
        if ((idx % board->side_len) != (board->side_len - 1)) {
            if (print_output) 
                printf("Pushed a tile to the left!\n");
            swap_tiles(board, idx, idx + 1);
            ret = true;
        } else {
            if (print_output)
                printf("Invalid move!\n");
        }
        break;                    
    default:
        // (shouldn't be ever accessed)
        fprintf(stderr, "Error: Invalid move direction!\n");
        exit(101);
    }
    return ret;
}

uint8_t get_progress(board_t* board) {
    uint8_t progress = 0;
    for (uint8_t i = 0; i < board->max_idx; i++) {
        if (board->tile_order[i] == i) {
            progress += 1;
        }
    }
    return progress;
}

bool is_solved(board_t* board) {
    uint8_t progress = get_progress(board);
    return (progress == board->max_idx + 1);
}
